<?php

include_once("simple_html_dom.php");

error_reporting(E_ALL ^ E_NOTICE);

function un_get_content_main_from_link( $link ) {

	# get the content from the static page template
	$content = file_get_contents( $link );

	$html = file_get_html($link);

	if (is_object($html)) {
		$ret = $html->find('div[id=main]');
		if (is_object($ret[0])) {
			return $ret[0]->innertext();
		}
	}

}

$url = $_GET["url"];

if ($url) {
	echo un_get_content_main_from_link($url);
}
